#!/bin/bash
#if [ -z "$(ls -A /usr/share/logstash/config)" ]; then
#    cp -a /.backup/logstash/config/* /usr/share/logstash/config/
#fi
rsync -av /.backup/logstash/config/* /usr/share/logstash/config/
if [ -z "$(ls -A /usr/share/logstash/pipeline |grep -v 'lost+found')" ]; then
    cp -aR /.backup/logstash/pipeline/* /usr/share/logstash/pipeline/
fi
