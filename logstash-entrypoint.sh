#!/bin/sh

set -e
/run/restore-config.sh
# Add logstash as command if needed
if [ "${1:0:1}" = '-' ]; then
	set -- logstash "$@"
fi

# Run as user "logstash" if the command is "logstash"
if [ "$1" = 'logstash' ]; then
	chown -R logstash: /usr/share/logstash
	set -- su-exec logstash "$@"
fi

exec "$@"
